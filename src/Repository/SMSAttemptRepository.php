<?php

namespace App\Repository;

use App\Entity\SMS;
use App\Entity\SMSAttempt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SMSAttempt|null find($id, $lockMode = null, $lockVersion = null)
 * @method SMSAttempt|null findOneBy(array $criteria, array $orderBy = null)
 * @method SMSAttempt[]    findAll()
 * @method SMSAttempt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SMSAttemptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SMSAttempt::class);
    }

    /**
     * Count number of attempts of each api, either failed or successful
     *
     * @param bool $are_successful specifies to count successful ones or failed ones
     * @param string|null $number if not null, count attempts for this number
     * @return array
     */
    public function apiAttemptsCount(bool $are_successful,?string $number = null)
    {
        $queryBuilder = $this->createQueryBuilder('s')
            ->select('count(s.id) attempts_count, s.api_endpoint')
            ->where('s.status = :val')
            ->setParameter('val', $are_successful ? SMSAttempt::SENT : SMSAttempt::FAILED);

        if ($number)
            $queryBuilder
                ->innerJoin(SMS::class, 'sms', Join::WITH, 'sms.id = s.sms')
                ->andWhere('sms.number = :number')
                ->setParameter('number', $number);

        $result = $queryBuilder
            ->groupBy('s.api_endpoint')
            ->getQuery()
            ->getResult();

        $response = [];
        foreach ($result as $item)
            $response[$item['api_endpoint']] = $item['attempts_count'];

        return $response;
    }
}
