<?php


namespace App\Service;


use App\Entity\SMS;
use App\Entity\SMSAttempt;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class Reporter
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    /**
     * @var AdapterInterface
     */
    private $cache;

    public function __construct(ManagerRegistry $managerRegistry, AdapterInterface $cache)
    {
        $this->doctrine = $managerRegistry;

        $this->cache = $cache;
    }

    /**
     * Create representation form of the report
     *
     * @param null $number
     * @return array
     */
    private function representation($number = null)
    {
        $attempts_repo = $this->doctrine->getRepository(SMSAttempt::class);
        $api_usage = $attempts_repo->apiAttemptsCount(SMSAttempt::SENT, $number);
        $api_error = $attempts_repo->apiAttemptsCount(SMSAttempt::FAILED, $number);

        $total_sent = array_sum($api_usage) + array_sum($api_error);

        $api_error_percentage = [];
        foreach ($api_error as $key => $value)
            if(isset($api_usage[$key]))
                $api_error_percentage[$key] = round(($value * 100) / $api_usage[$key], 2);

        $sms_repo = $this->doctrine->getRepository(SMS::class);
        $top_10 = $sms_repo->findTop10Numbers();

        return [
            'total_sms_count' => $total_sent,
            'api_usage_count' => $api_usage,
            'api_error_percentage' => $api_error_percentage,
            'top_10_most_used' => $top_10
        ];
    }

    /**
     * Load data either from cache or database
     *
     * @param string $number
     * @return mixed
     */
    public function loadData(?string $number = null)
    {
        $key = $number ? $number : 'report';
        $item = $this->cache->getItem($key);

        if (!$item->isHit())
        {
            $item->set($this->representation($number));
            $item->expiresAfter(new \DateInterval('PT5M'));
            $this->cache->save($item);
        }
        return $item->get();
    }

}