<?php

namespace App\Command;

use App\Entity\SMS;
use App\Service\SMSHandler;
use Enqueue\Redis\RedisConnectionFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\SerializerInterface;

class HandleFailedSmsCommand extends Command
{
    protected static $defaultName = 'handle-failed-sms';

    private $SMSHandler;

    private $redis_context;

    private $failed_queue;

    private $serializer;

    public function __construct(SMSHandler $SMSHandler, SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->SMSHandler = $SMSHandler;

        $factory = new RedisConnectionFactory();
        $this->redis_context = $factory->createContext();
        $this->failed_queue = $this->redis_context->createQueue('FailedSMS');

        $this->serializer = $serializer;

        parent::__construct(self::$defaultName);
    }

    protected function configure()
    {
        $this->setDescription('Get failed SMS from queue and resend them');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $consumer = $this->redis_context->createConsumer($this->failed_queue);

        while ($message = $consumer->receiveNoWait()) {
            $sms = $this->serializer->deserialize($message->getBody(), SMS::class, 'json');
            $this->SMSHandler->sendSMS($sms);
        }

        return 0;
    }
}
