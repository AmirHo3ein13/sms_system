<?php

namespace App\Repository;

use App\Entity\SMS;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SMS|null find($id, $lockMode = null, $lockVersion = null)
 * @method SMS|null findOneBy(array $criteria, array $orderBy = null)
 * @method SMS[]    findAll()
 * @method SMS[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SMSRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SMS::class);
    }

    /**
     * Find top 10 used numbers to send SMS
     *
     * @return int|mixed|string
     */
    public function findTop10Numbers()
    {
        return $this->createQueryBuilder('s')
            ->select('count(s.id) usage_count, s.number')
            ->groupBy('s.number')
            ->orderBy('count(s.id)', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}
