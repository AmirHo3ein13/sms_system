<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200611084823 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE sms_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE smsattempt_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE sms (id INT NOT NULL, number VARCHAR(14) NOT NULL, body TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE smsattempt (id INT NOT NULL, sms_id INT NOT NULL, api_endpoint SMALLINT NOT NULL, status BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F4877B75BD5C7E60 ON smsattempt (sms_id)');
        $this->addSql('ALTER TABLE smsattempt ADD CONSTRAINT FK_F4877B75BD5C7E60 FOREIGN KEY (sms_id) REFERENCES sms (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE smsattempt DROP CONSTRAINT FK_F4877B75BD5C7E60');
        $this->addSql('DROP SEQUENCE sms_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE smsattempt_id_seq CASCADE');
        $this->addSql('DROP TABLE sms');
        $this->addSql('DROP TABLE smsattempt');
    }
}
