<?php


namespace App\Service;


use App\Entity\SMS;
use App\Entity\SMSAttempt;
use Doctrine\Persistence\ManagerRegistry;
use Enqueue\Redis\RedisConnectionFactory;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SMSHandler
{
    private const DEFAULT_API_INDEX = 1;

    private const API_INDEX_KEY = 'APIIndex';

    private const FAILED_QUEUE_NAME = 'FailedSMS';

    private const DELIVERY_DELAY = 5 * 60 * 1000;

    /**
     * @var int
     */
    private $apiIndex;

    /**
     * @var \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    private $client;

    /**
     * Array of api endpoints
     *
     * @var string[]
     */
    private $apiEndpoints = [
        1 => 'http://localhost:81',
        2 => 'http://localhost:82',
    ];

    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    /**
     * @var \Enqueue\Redis\RedisContext|\Interop\Queue\Context
     */
    private $redisContext;

    /**
     * Reference of failed messages queue
     *
     * @var \Enqueue\Redis\RedisDestination|\Interop\Queue\Queue
     */
    private $failedQueue;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var AdapterInterface
     */
    private $cache;

    public function __construct(ManagerRegistry $managerRegistry, SerializerInterface $serializer, AdapterInterface $cache)
    {
        $this->doctrine = $managerRegistry;
        $this->client = HttpClient::create();

        $factory = new RedisConnectionFactory();
        $this->redisContext = $factory->createContext();
        $this->failedQueue = $this->redisContext->createQueue(self::FAILED_QUEUE_NAME);

        $this->serializer = $serializer;

        $this->cache = $cache;

        $this->apiIndex = $this->loadApiIndex();
    }

    /**
     * Send a SMS to the specified api
     *
     * @param string $api
     * @param SMS $sms
     * @return bool
     */
    private function sendRequest(string $api, SMS $sms)
    {
        try {
            $response = $this->client->request('POST', "$api/send/sms/?number={$sms->getNumber()}&body={$sms->getBody()}");
        } catch (TransportExceptionInterface $e) {
            return false;
        }

        if ($response->getStatusCode() > 299) {
            return false;
        }

        return true;
    }

    /**
     * Send a SMS to api
     *
     * @param SMS $sms
     */
    public function sendSMS(SMS $sms)
    {
        if ($this->sendRequest($this->getApiEndpoint(), $sms)) {
            $this->addAttempt($sms, $this->apiIndex, true);
            return;
        } else {
            $this->addAttempt($sms, $this->apiIndex, false);

            $this->flipApiIndex();

            if ($this->sendRequest($this->getApiEndpoint(), $sms)) {
                $this->addAttempt($sms, $this->apiIndex, true);
                return;
            } else {
                $this->addAttempt($sms, $this->apiIndex, false);
                $this->enqueueFailedSMS($sms);
                return;
            }
        }
    }

    /**
     * Send a new SMS
     *
     * @param string $number
     * @param string $body
     */
    public function sendNewSMS(string $number, string $body)
    {
        $sms = $this->createAndSaveSMS($number, $body);

        $this->sendSMS($sms);
    }

    /**
     * Create and save a SMS
     *
     * @param string $number
     * @param string $body
     * @return SMS|null
     */
    private function createAndSaveSMS(string $number, string $body): ?SMS
    {
        $sms = new SMS();
        $sms->setBody($body)
            ->setNumber($number);

        $this->saveObject($sms);

        return $sms;
    }

    /**
     * Add an attempt for sending a SMS
     *
     * @param SMS $sms
     * @param int $api the api index used to send the SMS
     * @param bool $is_sent is it sent or failed
     * @return SMSAttempt
     */
    private function addAttempt(SMS $sms, int $api, bool $is_sent)
    {
        $attempt = new SMSAttempt();
        $attempt->setApiEndpoint($api)->setSms($sms);
        $is_sent ? $attempt->markAsSent() : $attempt->markAsFailed();

        $this->saveObject($attempt);

        return $attempt;
    }

    /**
     * Save entity object
     *
     * @param $object
     */
    private function saveObject($object)
    {
        $entityManager = $this->doctrine->getManager();
        $entityManager->persist($object);
        $entityManager->flush();
    }

    /**
     * Getter for api endpoint
     *
     * @return string
     */
    public function getApiEndpoint(): string
    {
        return $this->apiEndpoints[$this->apiIndex];
    }

    /**
     * Change using api index to another one and save it to cache
     */
    private function flipApiIndex(): void
    {
        $this->apiIndex = ($this->apiIndex == 1) ? 2 : 1;
        $item = $this->cache->getItem(self::API_INDEX_KEY);
        $item->set($this->apiIndex);
        $this->cache->save($item);
    }

    /**
     * Load API Index from cache
     *
     * @return int
     */
    private function loadApiIndex(): int
    {
        $item = $this->cache->getItem(self::API_INDEX_KEY);

        if (!$item->isHit()) {
            $item->set(self::DEFAULT_API_INDEX);
            $this->cache->save($item);
        }
        return $item->get();
    }

    /**
     * Add failed SMS to the failed SMS queue
     *
     * @param SMS $sms
     */
    private function enqueueFailedSMS(SMS $sms)
    {
        $serialized_object = $this->serializer->serialize($sms, 'json');
        $message = $this->redisContext->createMessage($serialized_object);
        $this->redisContext
            ->createProducer()
            ->setDeliveryDelay(self::DELIVERY_DELAY)
            ->send($this->failedQueue, $message);
    }
}