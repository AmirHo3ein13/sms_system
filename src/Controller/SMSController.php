<?php


namespace App\Controller;


use App\Service\Reporter;
use App\Service\SMSHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SMSController extends AbstractController
{
    private $SMSHandler;
    private $reporter;
    public function __construct(SMSHandler $SMSHandler, Reporter $reporter)
    {
        $this->SMSHandler = $SMSHandler;
        $this->reporter = $reporter;
    }

    public function sendSMS(Request $request)
    {
        $body = $request->query->get('body');
        $number = $request->query->get('number');

        $this->SMSHandler->sendNewSMS($number, $body);

        return new Response();
    }

    public function report()
    {
        $data = $this->reporter->loadData();

        return $this->render('report.html.twig', $data);
    }

    public function searchNumber(Request $request)
    {
        $data = $this->reporter->loadData($request->get('number'));

        return $this->render('report.html.twig', $data);
    }
}