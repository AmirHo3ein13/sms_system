<?php

namespace App\Entity;

use App\EntityBase\EntityBase;
use App\Repository\SMSAttemptRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SMSAttemptRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class SMSAttempt extends EntityBase
{
    const FAILED = 0;
    const SENT = 1;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $api_endpoint;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=SMS::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $sms;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApiEndpoint(): ?int
    {
        return $this->api_endpoint;
    }

    public function setApiEndpoint(int $api_endpoint): self
    {
        $this->api_endpoint = $api_endpoint;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function markAsSent(): self
    {
        $this->status = self::SENT;

        return $this;
    }

    public function markAsFailed(): self
    {
        $this->status = self::FAILED;

        return $this;
    }

    public function getSMS(): ?SMS
    {
        return $this->sms;
    }

    public function setSMS(?SMS $sms): self
    {
        $this->sms = $sms;

        return $this;
    }
}
