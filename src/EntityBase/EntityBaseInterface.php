<?php

namespace App\EntityBase;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * EntityBase Interface
 */
interface EntityBaseInterface
{
    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void;

    /**
     * Get createdAt
     *
     * @return null|DateTime
     */
    public function getCreatedAt(): ?DateTime;

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     * @return self
     */
    public function setCreatedAt(DateTime $createdAt);
}